<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'training' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6h?TDy]-+%x/Q0tHTVlBrOmoh4rWJJq9BHGQ!s=uYjAHc3=.d hr5e:W!2hz,[n.' );
define( 'SECURE_AUTH_KEY',  'bLom`vUx7v]=:<Bi$Jr_fRgCde(Z)~_!>3&k;60,45&TDQ2>,*F4kM&O*mp=)%CF' );
define( 'LOGGED_IN_KEY',    'PBeO?:M]DNpaE+WczTCz,*|B`^.b6rh7$BYh>l.o]Wc*5Qyr(} FfhMM]H<H@[2&' );
define( 'NONCE_KEY',        'E/dfl-}U:Lae]~Yk/UrpJV4@2(2:z2Lq[MBQMg}H~R@[OOr|3WA<ZuF##1d)_jL5' );
define( 'AUTH_SALT',        'G+aqroC@G9Kw5:(_1sBR^Shd(Z&QR(9cGZX|H6+RmzYy/0*p%Q0dB9J1 veODX71' );
define( 'SECURE_AUTH_SALT', '!#)aL+1pjBv38N+mOR>--*%;@*4X?Pt`^818B9QbJgw.>AWC4P=;<D#[4[(>_|PN' );
define( 'LOGGED_IN_SALT',   'VF_&.@Jz8dKT}p3KRK,|oE6,w8n#$b+[G.CiWzQ/i)j3R`PBrV[!ZJ^j8Zzzf!E ' );
define( 'NONCE_SALT',       'zU6[F>cm.ori%n(f9Lk:@@NI)gVxT[,t.n0O,mSsGEt>l[X)T!m/Nv{sy`i8-B2)' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
